﻿using Needle.Core;
using Needle.Core.Dictionaries;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Windows.ApplicationModel.Background;
using Windows.Foundation.Collections;
using Windows.Media;
using Windows.Media.Playback;
using Windows.Storage;
using Windows.UI.Core;

namespace BackgroundAudioTask
{
    public enum PlayerCommands : int
    {
        Next,
        Prev,
        Start,
        Stop
    };
    public sealed class Task : IBackgroundTask
    {
        private SystemMediaTransportControls systemmediatransportcontrol;
        private AutoResetEvent backgroundTaskStarted = new AutoResetEvent(false);
        private BackgroundTaskDeferral _deferral;
        public async void Run(IBackgroundTaskInstance taskInstance)
        {
            _deferral = taskInstance.GetDeferral();
            await BackgroundExecutionManager.RequestAccessAsync();
            systemmediatransportcontrol = SystemMediaTransportControls.GetForCurrentView();
            systemmediatransportcontrol.ButtonPressed += systemmediatransportcontrol_ButtonPressed;
            systemmediatransportcontrol.PropertyChanged += systemmediatransportcontrol_PropertyChanged;
            systemmediatransportcontrol.IsEnabled = true;
            systemmediatransportcontrol.IsPauseEnabled = true;
            systemmediatransportcontrol.IsPlayEnabled = true;
            systemmediatransportcontrol.IsNextEnabled = true;
            systemmediatransportcontrol.IsPreviousEnabled = true;

            taskInstance.Canceled += TaskInstance_Canceled;

            BackgroundMediaPlayer.MessageReceivedFromForeground += BackgroundMediaPlayer_MessageReceivedFromForeground;
            BackgroundMediaPlayer.Current.CurrentStateChanged += Current_CurrentStateChanged;
            backgroundTaskStarted.Set();

            // var res = AppCore.InitializationData();
            if (AppCore.ViewModel_LocalData.ListFiles == null || !AppCore.ViewModel_LocalData.ListFiles.Any())
            {
                await AppCore.ViewModel_LocalData.GetFilesInSource();
                var s = AppCore.ViewModel_ListSongs.ListSongs.Count;
            }
        }

        private void systemmediatransportcontrol_PropertyChanged(SystemMediaTransportControls sender, SystemMediaTransportControlsPropertyChangedEventArgs args)
        {
            //throw new NotImplementedException();
        }
        private void systemmediatransportcontrol_ButtonPressed(SystemMediaTransportControls sender, SystemMediaTransportControlsButtonPressedEventArgs args)
        {
            var message = new ValueSet();
            switch (args.Button)
            {
                case SystemMediaTransportControlsButton.Play:
                    BackgroundMediaPlayer.Current.Play();
                    break;
                case SystemMediaTransportControlsButton.Pause:
                    BackgroundMediaPlayer.Current.Pause();
                    break;
                case SystemMediaTransportControlsButton.Next:

                    systemmediatransportcontrol.PlaybackStatus = MediaPlaybackStatus.Changing;
                    AppCore.ViewModel_CurrentPlayer.NextSong();
                    BackgroundMediaPlayer.Current.SetFileSource(AppCore.ViewModel_PropertiesActiveSong.ActiveFile);
                    BackgroundMediaPlayer.Current.Play();

                    message = new ValueSet();
                    message.Add(PlayerCommands.Start.ToString(), AppCore.ViewModel_PropertiesActiveSong.ActiveFile.Path);
                    BackgroundMediaPlayer.SendMessageToForeground(message);
                    break;
                case SystemMediaTransportControlsButton.Previous:
                    AppCore.ViewModel_CurrentPlayer.PreviousSong();
                    BackgroundMediaPlayer.Current.SetFileSource(AppCore.ViewModel_PropertiesActiveSong.ActiveFile);
                    BackgroundMediaPlayer.Current.Play();

                    message = new ValueSet();
                    message.Add(PlayerCommands.Start.ToString(), AppCore.ViewModel_PropertiesActiveSong.ActiveFile.Path);
                    BackgroundMediaPlayer.SendMessageToForeground(message);
                    break;
            }
        }
        private void Current_CurrentStateChanged(MediaPlayer sender, object args)
        {
            if (sender.CurrentState == MediaPlayerState.Playing)
            {
                systemmediatransportcontrol.PlaybackStatus = MediaPlaybackStatus.Playing;
                UpdateUVCOnNewTrack();
            }
            else if (sender.CurrentState == MediaPlayerState.Paused)
            {
                systemmediatransportcontrol.PlaybackStatus = MediaPlaybackStatus.Paused;
            }
        }
        private void BackgroundMediaPlayer_MessageReceivedFromForeground(object sender, MediaPlayerDataReceivedEventArgs e)
        {
            foreach (var obj in e.Data)
            {
                var value = obj.Value;
                PlayerCommands command;
                Enum.TryParse(obj.Key, out command);
                //switch (command)
                //{
                //case PlayerCommands.Start:
                AppCore.ViewModel_PropertiesActiveSong.SetActiveFileWithPlay(AppCore.ViewModel_LocalData.ListFiles.FirstOrDefault(x => x.Path.Equals(value.ToString())));
                if (AppCore.ViewModel_PropertiesActiveSong.ActiveFile == null)
                    return;

                var state = ApplicationSettingsHelper.ReadResetSettingsValue(ApplicationStates.AppState);
                if (state == null || (state != null && state.ToString().Equals(ApplicationStates.ForegroundAppActive)))
                    BackgroundMediaPlayer.Current.SetFileSource(AppCore.ViewModel_PropertiesActiveSong.ActiveFile);

                if (!BackgroundMediaPlayer.Current.CurrentState.Equals(MediaPlayerState.Playing))
                    BackgroundMediaPlayer.Current.Play();


                //   break;

                //}
            }
        }


        private void TaskInstance_Canceled(IBackgroundTaskInstance sender, BackgroundTaskCancellationReason reason)
        {
            _deferral.Complete();
        }


        private async void UpdateUVCOnNewTrack()
        {

            var state = ApplicationSettingsHelper.ReadResetSettingsValue(ApplicationStates.AppState);
            if (state == null || (state != null && !state.ToString().Equals(ApplicationStates.ForegroundAppSuspended)))
                return;

            await Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
            async () =>
            {
                var prop = await AppCore.ViewModel_PropertiesActiveSong?.ActiveFile?.Properties?.GetMusicPropertiesAsync();
                systemmediatransportcontrol.PlaybackStatus = MediaPlaybackStatus.Playing;
                systemmediatransportcontrol.DisplayUpdater.Type = MediaPlaybackType.Music;
                systemmediatransportcontrol.DisplayUpdater.MusicProperties.Title = prop?.Title ?? "Noname track";
                systemmediatransportcontrol.DisplayUpdater.MusicProperties.Artist = prop?.Artist ?? "Noartist track";
                systemmediatransportcontrol.DisplayUpdater.Update();
            }
        );

        }
    }
}
