﻿using Needle.Core;
using Windows.Foundation.Collections;
using Windows.Media.Playback;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

// Документацию по шаблону элемента пустой страницы см. по адресу http://go.microsoft.com/fwlink/?LinkID=390556

namespace Needle.WindowsPhone.View
{
    public delegate void AnimationPauseStartBegin(bool isPause);
    /// <summary>
    /// Пустая страница, которую можно использовать саму по себе или для перехода внутри фрейма.
    /// </summary>
    public sealed partial class PlayerPage : Page
    {  
        public static AnimationPauseStartBegin AnimationPauseStartBegin;
        public PlayerPage()
        {
            this.InitializeComponent();
            AnimationPauseStartBegin = (bool isPause) =>
            {
                if (isPause)
                    Animation_Pause.Begin();
                else
                    Animation_Start.Begin();
            };
        }

        /// <summary>
        /// Вызывается перед отображением этой страницы во фрейме.
        /// </summary>
        /// <param name="e">Данные события, описывающие, каким образом была достигнута эта страница.
        /// Этот параметр обычно используется для настройки страницы.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }
        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            SetInforation();
            btn_StartStop.Content = AppCore.ViewModel_CurrentPlayer.IconToBtnStartStop;
            btn_Shuf.IsChecked = AppCore.ViewModel_CurrentPlayer.GetShuffleMode();
            btn_Shuf.Checked += btn_Shuf_Checked;
            btn_Shuf.Unchecked += btn_Shuf_Unchecked;
            btn_Repeat.IsChecked = AppCore.ViewModel_CurrentPlayer.GetRepeatOneMode();
            btn_Repeat.Checked += btn_Repeat_Checked;
            btn_Repeat.Unchecked += btn_Repeat_Unchecked;


            btn_Favorite.IsChecked = AppCore.ViewModel_PropertiesActiveSong.IsFavorite();
            btn_Favorite.Content = ""; //пустое сердце
            if (btn_Favorite.IsChecked == true)
                btn_Favorite.Content = "";
            btn_Favorite.Checked += Btn_Favorite_Checked;
            btn_Favorite.Unchecked += Btn_Favorite_Unchecked;
        }


        private async void SetInforation()
        {
            SongImage.Background = await AppCore.ViewModel_PropertiesActiveSong.GetThumbnailSong();
            SongAlbum.Text = await AppCore.ViewModel_PropertiesActiveSong.GetAlbumSong();
            SongArtist.Text = await AppCore.ViewModel_PropertiesActiveSong.GetAuthorSong();
            SongTitle.Text = await AppCore.ViewModel_PropertiesActiveSong.GetTitleSong();
        }

        private void btn_Prev_Click(object sender, RoutedEventArgs e)
        {
            AppCore.ViewModel_CurrentPlayer.PreviousSong();
            SetInforation();
        }
        private void btn_Next_Click(object sender, RoutedEventArgs e)
        {
            AppCore.ViewModel_CurrentPlayer.NextSong();
            SetInforation();
        }
        private void btn_StartStop_Click(object sender, RoutedEventArgs e)
        {
            AppCore.ViewModel_CurrentPlayer.PlayPauseSong();
            btn_StartStop.Content = AppCore.ViewModel_CurrentPlayer.IconToBtnStartStop;

        }
        private void btn_Shuf_Checked(object sender, RoutedEventArgs e)
        {
            AppCore.ViewModel_CurrentPlayer.SetShuffleMode();
        }
        private void btn_Shuf_Unchecked(object sender, RoutedEventArgs e)
        {
            AppCore.ViewModel_CurrentPlayer.SetShuffleMode();
        }
        private void Btn_Favorite_Unchecked(object sender, RoutedEventArgs e)
        {
            var hashcode = AppCore.ViewModel_LocalData.GetHashCode(AppCore.ViewModel_PropertiesActiveSong.ActiveFile);
            var result = AppCore.ViewModel_PlayList.RemoveSong_PlayList(Dictionaries.PlaylistsGuid.FavoritePlayListGuid, hashcode);
            if (!result)
            {
                btn_Favorite.Checked -= Btn_Favorite_Checked;
                btn_Favorite.IsChecked = !btn_Favorite.IsChecked;
                btn_Favorite.Checked += Btn_Favorite_Checked;
                return;
            }
            btn_Favorite.Content = "";
        }
        private void Btn_Favorite_Checked(object sender, RoutedEventArgs e)
        {
            var hashcode = AppCore.ViewModel_LocalData.GetHashCode(AppCore.ViewModel_PropertiesActiveSong.ActiveFile);
            var result = AppCore.ViewModel_PlayList.AddSong_PlayList(Dictionaries.PlaylistsGuid.FavoritePlayListGuid, hashcode);
            if (!result)
            {
                btn_Favorite.Unchecked -= Btn_Favorite_Unchecked;
                btn_Favorite.IsChecked = !btn_Favorite.IsChecked;
                btn_Favorite.Unchecked += Btn_Favorite_Unchecked;
                return;
            }
            btn_Favorite.Content = ""; //пустое сердце
        }

        private void btn_Repeat_Unchecked(object sender, RoutedEventArgs e)
        {
            AppCore.ViewModel_CurrentPlayer.SetRepeatOneMode();
        }
        private void btn_Repeat_Checked(object sender, RoutedEventArgs e)
        {
            AppCore.ViewModel_CurrentPlayer.SetRepeatOneMode();
        }
    }
}
