﻿using Needle.Core;
using Needle.Core.Dictionaries;
using Needle.Shared.ViewModel.Other;
using Needle.WindowsPhone.View;
using System;
using System.Linq;
using Windows.ApplicationModel;
using Windows.Foundation.Collections;
using Windows.Media.Playback;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

// Документацию по шаблону элемента "Пустая страница" см. по адресу http://go.microsoft.com/fwlink/?LinkId=391641

namespace Needle.WindowsPhone
{
    public delegate void ChangeEnableButtonPlayer();
    /// <summary>
    /// Пустая страница, которую можно использовать саму по себе или для перехода внутри фрейма.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public static ChangeEnableButtonPlayer ChangeEnableButtonPlayer;
        public MainPage()
        {
            this.InitializeComponent();
            this.NavigationCacheMode = NavigationCacheMode.Required;
            ChangeEnableButtonPlayer = () =>
            {
                btn_Player.Enable(false);
                if (BackgroundMediaPlayer.Current.CurrentState == MediaPlayerState.Playing)
                    btn_Player.Enable(true);
            };
        }

        /// <summary>
        /// Вызывается перед отображением этой страницы во фрейме.
        /// </summary>
        /// <param name="e">Данные события, описывающие, каким образом была достигнута эта страница.
        /// Этот параметр обычно используется для настройки страницы.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // TODO: Подготовьте здесь страницу для отображения.

            // TODO: Если приложение содержит несколько страниц, обеспечьте
            // обработку нажатия аппаратной кнопки "Назад", выполнив регистрацию на
            // событие Windows.Phone.UI.Input.HardwareButtons.BackPressed.
            // Если вы используете NavigationHelper, предоставляемый некоторыми шаблонами,
            // данное событие обрабатывается для вас.

        }

        private async void Page_Loaded(object sender, RoutedEventArgs e)
        {
            ChangeEnableButtonPlayer();
            if (await AppCore.ViewModel_LocalData.GetFilesInSource())

                if (ListView_Music.ItemsSource == null)
                    ListView_Music.ItemsSource = AppCore.ViewModel_ListSongs.ListSongs;
          

            btn_Favorite.DataContext = CreateContextForButtonInMenu.CreateContext("", "ИЗБРАННОЕ", 0, Visibility.Visible);
            if (AppCore.ViewModel_PlayList.PlayLists.FirstOrDefault(x => x.ID.Equals(Dictionaries.PlaylistsGuid.FavoritePlayListGuid)) != null)
                btn_Favorite.DataContext = CreateContextForButtonInMenu.CreateContext("", "ИЗБРАННОЕ", AppCore.ViewModel_PlayList.PlayLists.FirstOrDefault(x => x.ID.Equals(Dictionaries.PlaylistsGuid.FavoritePlayListGuid)).CountItems, Visibility.Visible);
          
            btn_Account.DataContext = CreateContextForButtonInMenu.CreateContext("", "АККАУНТ");
            btn_NewPlayList.DataContext = CreateContextForButtonInMenu.CreateContext("", "НОВЫЙ ПЛЕЙЛИСТ");
            btn_Settings.DataContext = CreateContextForButtonInMenu.CreateContext("", "НАСТРОЙКИ");
        }
        private async void ListView_Music_ItemClick(object sender, ItemClickEventArgs e)
        {
            var file = AppCore.ViewModel_LocalData.ListFiles.FirstOrDefault(x => x.Path.Equals((e.ClickedItem as Model.Structure_ListSongs).Path));
            AppCore.ViewModel_PropertiesActiveSong.SetActiveFileWithPlay(file);

            var message = new ValueSet();
          //  var s = ViewModel.BusinessLayer.PlayerCommands.Start.ToString();
         
            message.Add("Start", AppCore.ViewModel_PropertiesActiveSong.ActiveFile.Path);

          
            BackgroundMediaPlayer.SendMessageToBackground(message);
            ChangeEnableButtonPlayer();

        }
        private void btn_Menu_Click(object sender, RoutedEventArgs e)
        {
            if (Grid_Menu.Visibility == Visibility.Visible)
                Storyboard_Menu_Close.Begin();
            else
                Storyboard_Menu_Open.Begin();
        }
        private void tgl_Vk_Checked(object sender, RoutedEventArgs e)
        {
            //App.ViewModelVkontakte.Authorize();
            //App.ViewModelVkontakte.GetAudio();
        }
        private void btn_Favorite_Click(object sender, RoutedEventArgs e)
        {
            AppCore.ViewModel_CurrentPlayer.ActivePlayList = Dictionaries.PlaylistsGuid.FavoritePlayListGuid;
            if(AppCore.ViewModel_ListSongs.ListSongs.Any())
                ListView_Music.ItemsSource = AppCore.ViewModel_ListSongs.ListSongs;
           
        }

        private void btn_Player_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(PlayerPage));
        }
    }
}
