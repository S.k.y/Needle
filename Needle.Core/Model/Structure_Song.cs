﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Needle.Model
{
    public class Structure_Song : Interface.NotifyPropertyChangedBase
    {

        public virtual Guid IdPlayList { get; set; }
        public int HashCodeSong { get; set; }

    }
}
