﻿using System;
using System.Collections.Generic;
using System.Text;
using Windows.Storage;
using Windows.UI.Xaml;

namespace Needle.Model
{
    public class Structure_ListSongs : Structure_Song
    {
        public string Title { get; set; }
        public int Number { get; set; }
        public string NumberStr
        {
            get { return Number.ToString("00"); }
        }

        public string Artist { get; set; }
        public string Path { get; set; }

        private Visibility _isActive = Visibility.Collapsed;
        public Visibility IsActive
        {
            get
            {           
                return _isActive;
            }
            set
            {
                _isActive = value;
                RaisePropertyChanged("IsActive");
            }
        }

        public override Guid IdPlayList
        {
            get
            {
                return base.IdPlayList;
            }

            set
            {
                base.IdPlayList = value;
                RaisePropertyChanged("IdPlayList");
            }
        }
    }
}
