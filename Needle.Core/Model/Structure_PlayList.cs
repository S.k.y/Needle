﻿
using SQLite;
using System;
using System.Collections.ObjectModel;

namespace Needle.Model
{
    public class Structure_PlayList
    {
        [PrimaryKey]
        public Guid ID { get; set; }
        public string Name { get; set; }
        [Ignore]
        public int CountItems { get; set; }
        
    }
}
