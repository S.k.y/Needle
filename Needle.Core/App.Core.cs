﻿using System.Threading.Tasks;

namespace Needle.Core
{
    public class AppCore
    {

        public static async Task<bool> InitializationData()
        {
            _viewModel_DataBase = new ViewModel.DataLayer.ViewModel_DataBase();
            return await ViewModel_LocalData.GetFilesInSource();
        

        }
        private static ViewModel.DataLayer.ViewModel_LocalData _viewModel_LocalData;
        public static ViewModel.DataLayer.ViewModel_LocalData ViewModel_LocalData
        {
            get
            {
                if (_viewModel_LocalData == null)
                    _viewModel_LocalData = new ViewModel.DataLayer.ViewModel_LocalData();
                return _viewModel_LocalData;
            }
        }

        private static ViewModel.BusinessLayer.ViewModel_PropertiesActiveSong _viewModel_PropertiesActiveSong;
        public static ViewModel.BusinessLayer.ViewModel_PropertiesActiveSong ViewModel_PropertiesActiveSong
        {
            get
            {
                if (_viewModel_PropertiesActiveSong == null)
                    _viewModel_PropertiesActiveSong = new ViewModel.BusinessLayer.ViewModel_PropertiesActiveSong();
                return _viewModel_PropertiesActiveSong;
            }
        }

        private static ViewModel.BusinessLayer.ViewModel_Songs _viewModel_ListSongs;
        public static ViewModel.BusinessLayer.ViewModel_Songs ViewModel_ListSongs
        {
            get
            {
                if (_viewModel_ListSongs == null)
                    _viewModel_ListSongs = new ViewModel.BusinessLayer.ViewModel_Songs();
                return _viewModel_ListSongs;
            }
        }

        private static ViewModel.BusinessLayer.ViewModel_CurrentPlayer _viewModel_CurrentPlayer;
        public static ViewModel.BusinessLayer.ViewModel_CurrentPlayer ViewModel_CurrentPlayer
        {
            get
            {
                if (_viewModel_CurrentPlayer == null)
                    _viewModel_CurrentPlayer = new ViewModel.BusinessLayer.ViewModel_CurrentPlayer();
                return _viewModel_CurrentPlayer;
            }
        }

        private static ViewModel.BusinessLayer.ViewModel_PlayList _viewModel_PlayList;
        public static ViewModel.BusinessLayer.ViewModel_PlayList ViewModel_PlayList
        {
            get
            {
                if (_viewModel_PlayList == null)
                    _viewModel_PlayList = new ViewModel.BusinessLayer.ViewModel_PlayList();

                return _viewModel_PlayList;
            }
        }

        private static ViewModel.DataLayer.ViewModel_DataBase _viewModel_DataBase;
        public static ViewModel.DataLayer.ViewModel_DataBase ViewModel_DataBase
        {
            get
            {
                if (_viewModel_DataBase == null)
                    _viewModel_DataBase = new ViewModel.DataLayer.ViewModel_DataBase();
                return _viewModel_DataBase;
            }
        }
    }
}
