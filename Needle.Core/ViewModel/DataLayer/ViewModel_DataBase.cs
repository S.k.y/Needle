﻿using Needle.Core;
using System;
using System.Diagnostics;
using System.IO;
using Windows.Storage;

namespace Needle.ViewModel.DataLayer
{
    public class ViewModel_DataBase
    {
        public static readonly string connectionStr = Path.Combine(ApplicationData.Current.LocalFolder.Path, "BasicDataBase.sqlite");
        public ViewModel_DataBase()
        {
            using (var db = new SQLite.SQLiteConnection(connectionStr))
            {
                db.CreateTable<Model.Structure_Song>();
                db.CreateTable<Model.Structure_PlayList>();
            }
            DataBase_Load();
        }
        /// <summary>
        /// Получаем данные из БД
        /// </summary>
        private void DataBase_Load()
        {
            using (var db = new SQLite.SQLiteConnection(connectionStr))
            {
                foreach (var item in db.Table<Model.Structure_Song>())
                    AppCore.ViewModel_PlayList.ListSongsInPlayLists.Add(item);

                foreach (var item in db.Table<Model.Structure_PlayList>())
                    AppCore.ViewModel_PlayList.PlayLists.Add(item);

                AppCore.ViewModel_PlayList.UpdateCount_PlayList();
            }
        }

        public bool DataBase_Insert(object item)
        {
            try
            {
                using (var db = new SQLite.SQLiteConnection(connectionStr))
                {
                    db.Insert(item);
                    return true;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error in DataBase_Insert: " + ex.Message);
                return false;
            }
        }
        public bool DataBase_Delete(object item)
        {
            try
            {
                using (var db = new SQLite.SQLiteConnection(connectionStr))
                {
                    db.Delete(item);
                    return true;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error in DataBase_Delete: " + ex.Message);
                return false;
            }
        }
    }
}
