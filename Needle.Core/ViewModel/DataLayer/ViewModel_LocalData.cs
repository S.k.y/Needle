﻿using Needle.Core;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Windows.Storage;

namespace Needle.ViewModel.DataLayer
{
    public class ViewModel_LocalData : Interface.IGetData<StorageFile>
    {
        private IReadOnlyList<StorageFile> _listFiles;
        public IReadOnlyList<StorageFile> ListFiles
        {
            get
            {
                return _listFiles;
            }
            protected set
            {
                _listFiles = value;
            }
        }
        public async Task<bool> GetFilesInSource()
        {
            try
            {
                if (ListFiles != null && ListFiles.Count > 0)
                    return false;

                var list = new List<StorageFile>();
                var folders = await KnownFolders.MusicLibrary.GetFoldersAsync();

                foreach (var folder in folders)
                    await RetriveFilesInFolders(list, folder);

                var listTypes = new List<string>() { ".mp3", ".m4a", ".flac", ".wav" };
                ListFiles = list.Where(x => listTypes.Contains(x.FileType)).ToList();
                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error in load local files: " + ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Перебор всех папок и получение файлов.
        /// </summary>
        /// <param name="list"></param>
        /// <param name="parent"></param>
        /// <returns></returns>
        private async Task RetriveFilesInFolders(List<StorageFile> list, StorageFolder parent)
        {
            foreach (var item in await parent.GetFilesAsync()) list.Add(item);
            foreach (var item in await parent.GetFoldersAsync()) await RetriveFilesInFolders(list, item);
        }



        /// <summary>
        /// Получение хеш-кода указанного файла.
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public int GetHashCode(StorageFile file)
        {
            return file.GetHashCode();
        }

        public StorageFile GeFileToIndex(Enum command)
        {
            var value = Convert.ToInt32(command);
            switch (value)
            {
                case (int)ViewModel.BusinessLayer.PlayerCommands.Next:
                    return ListFiles[AppCore.ViewModel_ListSongs.GetNextIndexSong(1)];

                case (int)ViewModel.BusinessLayer.PlayerCommands.Prev:
                    return ListFiles[AppCore.ViewModel_ListSongs.GetNextIndexSong(-1)];
                default:
                    return ListFiles[AppCore.ViewModel_ListSongs.GetNextIndexSong(0)];
            }

        }
    }
}

