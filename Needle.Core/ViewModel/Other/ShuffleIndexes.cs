﻿using System;
using System.Collections.Generic;
using Needle.Model;
using System.Linq;
using Needle.Interface;

namespace Needle.ViewModel.Other
{
    public class ShuffleIndexes : IShuffleIndexes<Structure_ListSongs>
    {
        private IShuffleIndexes<Structure_ListSongs> _instance;
        public IShuffleIndexes<Structure_ListSongs> Instance
        {
            get
            {
                if (_instance == null)
                    _instance = this;
                return _instance;
            }
        }
        private IList<int> _listIndexes;
        public IList<int> ListIndexes
        {
            get
            {
                return _listIndexes;
            }

            internal set
            {
                _listIndexes = value;
            }
        }

        private bool _isGetNextIndex;
        public bool IsGetNextIndex
        {
            get
            {
                return _isGetNextIndex;
            }
            internal set
            {
                _isGetNextIndex = value;
            }
        }
        public void CreateListIndexes(IList<Structure_ListSongs> listRecords)
        {
            var _tmpList = new List<int>();
            foreach (var item in listRecords)
                _tmpList.Add(item.Number);

            int n = _tmpList.Count;
            var rng = new Random();
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                var value = _tmpList[k];
                _tmpList[k] = _tmpList[n];
                _tmpList[n] = value;
            }
            ListIndexes = _tmpList;
            IsGetNextIndex = true;
        }
        public int GetNextIndex()
        {
            if(ListIndexes == null || !ListIndexes.Any())
            {
                IsGetNextIndex = false;
                return 0;
            }
            var index = ListIndexes.First();
            ListIndexes.Remove(index);
            return (index - 1);
        }
      
    }
}
