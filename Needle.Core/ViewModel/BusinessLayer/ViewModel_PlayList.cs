﻿using Needle.Core;
using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace Needle.ViewModel.BusinessLayer
{
    /// <summary>
    /// Класс для взаимодействия с плейлистами.
    /// Содержит методы для работы с плейлистами.
    /// </summary>
    public class ViewModel_PlayList : Interface.NotifyPropertyChangedBase
    {
        /// <summary>
        /// Список записей в бд о плейлистах и песнях.
        /// </summary>
        public ObservableCollection<Model.Structure_Song> ListSongsInPlayLists;
        private ObservableCollection<Model.Structure_PlayList> _playLists;
        public ObservableCollection<Model.Structure_PlayList> PlayLists
        {
            get
            {
                return _playLists;
            }
            set
            {
                _playLists = value;
                RaisePropertyChanged("PlayLists");
            }
        }
        public ViewModel_PlayList()
        {
            ListSongsInPlayLists = new ObservableCollection<Model.Structure_Song>();
            PlayLists = new ObservableCollection<Model.Structure_PlayList>();
        }
     
        /// <summary>
        /// Получение Id плейлиста по хеш-коду.
        /// </summary>
        /// <param name="hashcode">хеш-код</param>
        /// <returns></returns>
        public Guid Get_IdPlayList_ByHashCodeFile(int hashcode)
        {
            return ListSongsInPlayLists?.FirstOrDefault(x => x.HashCodeSong.Equals(hashcode))?.IdPlayList ?? Guid.Empty;
        }
        public bool IsFavoritePlayList_ByIdPlayList(Guid idPlaylist)
        {
            if (idPlaylist.Equals(Dictionaries.PlaylistsGuid.FavoritePlayListGuid))
                return true;
            return false;
        }

        /// <summary>
        /// Заполнение плейлистов данными
        /// </summary>
        public void UpdateCount_PlayList()
        {
            foreach (var list in PlayLists)
                list.CountItems = ListSongsInPlayLists.Count(x => x.IdPlayList.Equals(list.ID));
        }
    
    
        /// <summary>
        /// Создание нового плейлиста
        /// </summary>
        /// <param name="item"></param>
        public void Create_PlayList(Model.Structure_PlayList item)
        {
            if (AppCore.ViewModel_DataBase.DataBase_Insert(item))
                AppCore.ViewModel_PlayList.PlayLists.Add(item);
        }
        /// <summary>
        /// Добавление песни в пейлист
        /// </summary>
        /// <param name="guid">id плейлиста</param>
        /// <param name="hashcode">HashCode песни</param>
        /// <returns></returns>
        public bool AddSong_PlayList(Guid guid, int hashcode)
        {
            if (guid.Equals(Dictionaries.PlaylistsGuid.FavoritePlayListGuid))
                Create_PlayList_Favorite();

            var item = new Model.Structure_Song();
            item.HashCodeSong = hashcode;
            item.IdPlayList = guid;

            if (AppCore.ViewModel_DataBase.DataBase_Insert(item))
            {
                AppCore.ViewModel_PlayList.ListSongsInPlayLists.Add(item);
                AppCore.ViewModel_ListSongs.ChangeSong_IdPlayList(guid, hashcode);
                PlayLists.FirstOrDefault(x => x.ID.Equals(guid)).CountItems++;
                return true;
            }
            return false;
        }
        /// <summary>
        /// Удаление песни из плейлиста
        /// </summary>
        /// <param name="guid">id плейлиста</param>
        /// <param name="hashcode">HashCode песни</param>
        /// <returns></returns>
        public bool RemoveSong_PlayList(Guid guid, int hashcode)
        {
            var item = new Model.Structure_Song();
            item.HashCodeSong = hashcode;
            item.IdPlayList = guid;
            if (AppCore.ViewModel_DataBase.DataBase_Delete(item))
            {
                AppCore.ViewModel_PlayList.ListSongsInPlayLists.Remove(item);
                AppCore.ViewModel_ListSongs.ChangeSong_IdPlayList(Guid.Empty, hashcode);
                PlayLists.FirstOrDefault(x => x.ID.Equals(guid)).CountItems--;
                return true;
            }
            return false;
        }

        /// <summary>
        /// Создание плейлиста избранные
        /// </summary>
        /// <returns></returns>
        public Guid Create_PlayList_Favorite()
        {
            if (AppCore.ViewModel_PlayList.PlayLists == null || !AppCore.ViewModel_PlayList.PlayLists.Any() || AppCore.ViewModel_PlayList.PlayLists.FirstOrDefault(x => x.ID.Equals(Dictionaries.PlaylistsGuid.FavoritePlayListGuid)) == null)
            {
                var item = new Model.Structure_PlayList();
                item.ID = Dictionaries.PlaylistsGuid.FavoritePlayListGuid;
                item.Name = "Favorite";
                Create_PlayList(item);
                return item.ID;
            }
            return Guid.Empty;
        }

    }
}
