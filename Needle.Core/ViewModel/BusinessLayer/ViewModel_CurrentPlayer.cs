﻿using Needle.Core;
using System;
using Windows.Foundation.Collections;
using Windows.Media.Playback;
using Windows.UI.Xaml;

namespace Needle.ViewModel.BusinessLayer
{
    public enum PlayerCommands : int
    {
        Next,
        Prev,
        Start,
        Stop
    };
    public class ViewModel_CurrentPlayer : Interface.NotifyPropertyChangedBase
    {
        private bool IsShuffle = false;
        private bool IsRepeatOne = false;
        private Guid _activePlayList = Guid.Empty;
        public Guid ActivePlayList
        {
            get
            {
                return _activePlayList;
            }
            set
            {
                _activePlayList = value;
            }
        }

        private string _iconToBtnStartStop;
        public string IconToBtnStartStop
        {
            get
            {
                RaisePropertyChanged("IconbtnStartStop");
                if (String.IsNullOrWhiteSpace(_iconToBtnStartStop))
                    return "";
                return _iconToBtnStartStop;
            }
            internal set
            {
                _iconToBtnStartStop = value;
            }
        }

        public ViewModel_CurrentPlayer()
        {
           // Timer = new DispatcherTimer();
            //Timer.Interval = new TimeSpan(0, 0, 1);
            //Timer.Tick += Timer_Tick;
            
        }
        public void SetSongToMediaPlayer()
        {
            //var message = new ValueSet();
            //message.Add(ViewModel.BusinessLayer.PlayerCommands.Start.ToString(), AppCore.ViewModel_PropertiesActiveSong.ActiveFile.Path);
            //BackgroundMediaPlayer.SendMessageToBackground(message);
            //var obj = App.MediaElement;
            // obj.SetSource(await App.ViewModel_PropertiesActiveSong.GetStreamToFile(), "");


        }



        private void ChangeStateMediaPlayer()
        {
            switch (BackgroundMediaPlayer.Current.CurrentState)
            {
                case MediaPlayerState.Playing:
                    BackgroundMediaPlayer.Current.Pause();
                    IconToBtnStartStop = "";
                    break;
                case MediaPlayerState.Paused:
                    BackgroundMediaPlayer.Current.Play();
                    IconToBtnStartStop = "";
                    break;
            }
        }
        public void ShutdownMediaPlayer()
        {
            BackgroundMediaPlayer.Shutdown();
          
        }


        public void SetShuffleMode()
        {
            IsShuffle = !IsShuffle;
        }
        public void SetRepeatOneMode()
        {
            IsRepeatOne = !IsRepeatOne;
        }
        public bool GetShuffleMode()
        {
            return IsShuffle;
        }
        public bool GetRepeatOneMode()
        {
            return IsRepeatOne;
        }

        public void NextSong()
        {
            AppCore.ViewModel_PropertiesActiveSong.SetActiveFileWithPlay(AppCore.ViewModel_LocalData.GeFileToIndex(PlayerCommands.Next));
            var message = new ValueSet();
            message.Add(PlayerCommands.Start.ToString(), AppCore.ViewModel_PropertiesActiveSong.ActiveFile.Path);
            BackgroundMediaPlayer.SendMessageToBackground(message);
        }
        public void PreviousSong()
        {
            AppCore.ViewModel_PropertiesActiveSong.SetActiveFileWithPlay(AppCore.ViewModel_LocalData.GeFileToIndex(PlayerCommands.Prev));
            var message = new ValueSet();
            message.Add(PlayerCommands.Start.ToString(), AppCore.ViewModel_PropertiesActiveSong.ActiveFile.Path);
            BackgroundMediaPlayer.SendMessageToBackground(message);
        }
        public void PlayPauseSong()
        {
          
            AppCore.ViewModel_CurrentPlayer.ChangeStateMediaPlayer();
        }
    }
}
