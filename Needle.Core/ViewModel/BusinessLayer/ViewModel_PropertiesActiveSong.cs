﻿using System;
using Needle.Interface;
using Windows.Storage;
using System.Threading.Tasks;
using Windows.Storage.Streams;
using Windows.Storage.FileProperties;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Media;
using System.Linq;
using Needle.Core;

namespace Needle.ViewModel.BusinessLayer
{
    public class ViewModel_PropertiesActiveSong : IPropertiesForActiveSong<StorageFile>
    {
        private StorageFile _activeFile;
        private MusicProperties MusicProperties;
        public StorageFile ActiveFile
        {
            get
            {
                return _activeFile;
            }
            internal set
            {
                _activeFile = value;
            }
        }

        public async Task<string> GetAlbumSong()
        {
            if (MusicProperties == null)
                MusicProperties = await ActiveFile.Properties.GetMusicPropertiesAsync();
            return MusicProperties.Album;
        }
        public async Task<string> GetAuthorSong()
        {
            if (MusicProperties == null)
                MusicProperties = await ActiveFile.Properties.GetMusicPropertiesAsync();
            return MusicProperties.Artist;
        }
        public async Task<string> GetTitleSong()
        {
            if (MusicProperties == null)
                MusicProperties = await ActiveFile.Properties.GetMusicPropertiesAsync();
            return MusicProperties.Title;
        }
        public string GetPathToFile()
        {
            var path = String.Empty;
            var uri = new Uri(ActiveFile.Path, UriKind.Absolute);
            path = uri.AbsolutePath;
            path = System.Uri.UnescapeDataString(path);

            var paths = path.Replace('/', '\'');
            return path;
        }
        public async Task<ImageBrush> GetThumbnailSong()
        {
            var thumbnail = await ActiveFile.GetScaledImageAsThumbnailAsync(Windows.Storage.FileProperties.ThumbnailMode.SingleItem);
            var bm = new BitmapImage();
            if (thumbnail != null)
            {
                bm.SetSource(thumbnail);
                ImageBrush ib = new ImageBrush();
                ib.ImageSource = bm;
                return ib;
            }
            return new ImageBrush();
        }
        public int GetIndexToFile()
        {
            if (AppCore.ViewModel_LocalData.ListFiles != null && AppCore.ViewModel_LocalData.ListFiles.Any())
                return AppCore.ViewModel_LocalData.ListFiles.ToList().IndexOf(ActiveFile);
            return 0;
        }
        public async Task<IRandomAccessStream> GetStreamToFile()
        {
            var stream = await ActiveFile.OpenAsync(FileAccessMode.Read);
            return stream;
        }
        /// <summary>
        /// Установка активного трека с воспроизведением
        /// </summary>
        /// <param name="file"></param>
        public void SetActiveFileWithPlay(StorageFile file)
        {
            ActiveFile = file;
            MusicProperties = null;
           // AppCore.ViewModel_CurrentPlayer.ShutdownMediaPlayer();
            AppCore.ViewModel_CurrentPlayer.SetSongToMediaPlayer();
            AppCore.ViewModel_CurrentPlayer.PlayPauseSong();
            AppCore.ViewModel_ListSongs.ChangeSong_IsActive();

        }
        /// <summary>
        /// Установка активного трека без его воспроизведения.
        /// </summary>
        /// <param name="file"></param>
        public void SetActiveFileWithoutPlay(StorageFile file)
        {
            ActiveFile = file;
           // AppCore.ViewModel_CurrentPlayer.ShutdownMediaPlayer();
            AppCore.ViewModel_ListSongs.ChangeSong_IsActive();
        }
        public bool IsFavorite()
        {
            var hashcode = AppCore.ViewModel_LocalData.GetHashCode(ActiveFile);
            var idplaylist = AppCore.ViewModel_PlayList.Get_IdPlayList_ByHashCodeFile(hashcode);
            return AppCore.ViewModel_PlayList.IsFavoritePlayList_ByIdPlayList(idplaylist);
        }
    }
}
