﻿using Needle.Core;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using Windows.Storage;

namespace Needle.ViewModel.BusinessLayer
{
    /// <summary>
    /// Работа с листом песен. Его изменение и выбор необходимой песни.
    /// </summary>
    public class ViewModel_Songs: Interface.NotifyPropertyChangedBase
    {
        /// <summary>
        /// Список песен для отображения и взаимодействия.
        /// </summary>
        private ObservableCollection<Model.Structure_ListSongs> _listSongs;
        public ObservableCollection<Model.Structure_ListSongs> ListSongs
        {
            get
            {
                if (!AppCore.ViewModel_CurrentPlayer.ActivePlayList.Equals(Guid.Empty))
                {
                    return new ObservableCollection<Model.Structure_ListSongs>(_listSongs.Where(x => x.IdPlayList.Equals(AppCore.ViewModel_CurrentPlayer.ActivePlayList)));
                }
                return _listSongs;
            }
            internal set
            {
                _listSongs = value;
            }
        }

        /// <summary>
        /// Список индексаов для рандомного воспроизведения
        /// </summary>
        ViewModel.Other.ShuffleIndexes ListIndexes = new Other.ShuffleIndexes();
        public ViewModel_Songs()
        {
            Create_ListSongs(AppCore.ViewModel_LocalData.ListFiles);
        }

        /// <summary>
        /// Получение индекса следующей песни.
        /// </summary>
        /// <param name="increase">параметр для увеличения или уменьшения текущего индекса</param>
        /// <returns></returns>
        public int GetNextIndexSong(int increase)
        {
            if (AppCore.ViewModel_CurrentPlayer.GetShuffleMode())
            {
                if (ListIndexes.IsGetNextIndex == false)
                    ListIndexes.CreateListIndexes(ListSongs);
                return ListIndexes.GetNextIndex();
            }
            else
            {
                var index = AppCore.ViewModel_PropertiesActiveSong.GetIndexToFile();
                index += increase;
                if (index < 0) index = (ListSongs.Count - 1);
                if (index > (ListSongs.Count - 1)) index = 0;
                return index;
            }
        }

        private async void Create_ListSongs(IReadOnlyList<StorageFile> files)
        {
            try
            {
                ListSongs = new ObservableCollection<Model.Structure_ListSongs>();
                foreach (var file in files)
                {
                    var musicProperties = await file.Properties.GetMusicPropertiesAsync();
                    var hashcode = AppCore.ViewModel_LocalData.GetHashCode(file);
                    var idPlaylist = AppCore.ViewModel_PlayList.Get_IdPlayList_ByHashCodeFile(hashcode);
                    var item = new Model.Structure_ListSongs
                    {
                        Number = (ListSongs.Count + 1),
                        Artist = musicProperties.Artist,
                        Title = musicProperties.Title,
                        Path = file.Path,
                        HashCodeSong = hashcode,
                        IdPlayList = idPlaylist
                    };
                    ListSongs.Add(item);
                }
            }
            catch(Exception ex)
            {
                Debug.WriteLine("Error in create list songs: " + ex.Message);
            }
        }

        /// <summary>
        /// Изменение IdPlayList в песне во время удаления или добавления песни в плейлист.
        /// </summary>
        /// <param name="newIdPlayList"></param>
        /// <param name="hashcode"></param>
        public void ChangeSong_IdPlayList(Guid newIdPlayList, int hashcode)
        {
            if(ListSongs.FirstOrDefault(x => x.HashCodeSong.Equals(hashcode)) != null)
                ListSongs.FirstOrDefault(x => x.HashCodeSong.Equals(hashcode)).IdPlayList = newIdPlayList;
        }
      
        /// <summary>
        /// Изменение флага IsActive. Отвечает за отображение на списке песен активной песни.
        /// </summary>
        public void ChangeSong_IsActive()
        {
            if (ListSongs == null)
                return;

            var item = ListSongs.FirstOrDefault(x => x.IsActive == Windows.UI.Xaml.Visibility.Visible);
            if(item != null)
                item.IsActive = Windows.UI.Xaml.Visibility.Collapsed;

            ListSongs.FirstOrDefault(x => x.Path.Equals(AppCore.ViewModel_PropertiesActiveSong.ActiveFile.Path)).IsActive = Windows.UI.Xaml.Visibility.Visible;
        }

    }
}
