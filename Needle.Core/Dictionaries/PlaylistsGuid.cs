﻿using System;

namespace Needle.Dictionaries
{
    public static class PlaylistsGuid
    {
        public static readonly Guid FavoritePlayListGuid = Guid.Parse("00000000-0000-0000-0000-000000000001");
    }
}
