﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Needle.Core.Dictionaries
{
    public static class ApplicationStates
    {
        public const string AppState = "appstate";
        public const string AppSuspended = "appsuspend";
        public const string AppResumed = "appresumed";
        public const string ForegroundAppActive = "Active";
        public const string ForegroundAppSuspended = "Suspended";
    }
}
