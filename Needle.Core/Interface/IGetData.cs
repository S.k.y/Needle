﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Needle.Interface
{
    interface IGetData<T>
    {
        Task<bool> GetFilesInSource();
        IReadOnlyList<T> ListFiles { get; }

        T GeFileToIndex(Enum command);
    }
}
