﻿using Needle.Core;
using Needle.Core.Dictionaries;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Windows.UI.Core;
using Windows.UI.Xaml;

namespace Needle.Interface
{
    public class NotifyPropertyChangedBase: INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        protected void RaisePropertyChanged(string propertyName)
        {
            var state = ApplicationSettingsHelper.ReadResetSettingsValue(ApplicationStates.AppState);
            if (state == null || (state != null && state.ToString().Equals(ApplicationStates.ForegroundAppActive)))
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
