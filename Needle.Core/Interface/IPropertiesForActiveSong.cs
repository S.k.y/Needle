﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage.FileProperties;
using Windows.Storage.Streams;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;

namespace Needle.Interface
{
    interface IPropertiesForActiveSong<T>
    {
        T ActiveFile { get; }
        void SetActiveFileWithPlay(T file);
        Task<IRandomAccessStream> GetStreamToFile();
        string GetPathToFile();
        int GetIndexToFile();
        Task<ImageBrush> GetThumbnailSong();
        Task<string> GetTitleSong();
        Task<string> GetAuthorSong();
        Task<string> GetAlbumSong();
        bool IsFavorite();
    }
}
