﻿using Needle.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace Needle.Interface
{
    public interface IShuffleIndexes<T>
    {
        bool IsGetNextIndex { get; }
        IShuffleIndexes<T> Instance { get; }
        IList<int> ListIndexes { get; }
        int GetNextIndex();
        void CreateListIndexes(IList<T> listRecords);
    }
}
