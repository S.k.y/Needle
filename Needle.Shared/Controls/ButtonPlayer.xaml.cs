﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Шаблон элемента пользовательского элемента управления задокументирован по адресу http://go.microsoft.com/fwlink/?LinkId=234236

namespace Needle.Shared.Controls
{
    public sealed partial class ButtonPlayer : UserControl
    {
       
        public ButtonPlayer()
        {
            this.InitializeComponent();
        }
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
        
        }
        public async void Enable(bool isEnable)
        {
            if (isEnable)
            {
               await Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
               () =>
                   {
                       AnimationForFill.Begin();
                       MainBorder.Background = new SolidColorBrush((Color)Application.Current.Resources["Color_Acent"]);
                       rectangle.Visibility = rectangle1.Visibility = Visibility.Visible;
                   }
               );
            }
            else
            {
                await Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
                () =>
                    {
                        AnimationForFill.Stop();
                        MainBorder.Background = new SolidColorBrush((Color)Application.Current.Resources["Color_Background"]);
                        rectangle.Visibility = rectangle1.Visibility = Visibility.Collapsed;
                    }
                );

            }
        }
    }
}
