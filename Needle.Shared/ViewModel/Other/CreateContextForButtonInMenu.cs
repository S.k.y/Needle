﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;

namespace Needle.Shared.ViewModel.Other
{
    public static class CreateContextForButtonInMenu
    {
        public static object CreateContext(string icon, string name, int count, Visibility isCountVisibility)
        {
            var obj = new { Icon = icon, Name = name, Count = count, IsCountVisibility = isCountVisibility };
            return obj;
        }
        public static object CreateContext(string icon, string name)
        {
            var obj = new { Icon = icon, Name = name, Count = 0, IsCountVisibility = Visibility.Collapsed };
            return obj;
        }
    }
}
