﻿
using Needle.Core;
using Needle.Core.Dictionaries;
using Needle.WindowsPhone;
using System;
using System.Linq;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.Foundation.Collections;
using Windows.Media.Playback;
using Windows.Phone.UI.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;

// Документацию по шаблону "Пустое приложение" см. по адресу http://go.microsoft.com/fwlink/?LinkId=391641

namespace Needle
{
    /// <summary>
    /// Обеспечивает зависящее от конкретного приложения поведение, дополняющее класс Application по умолчанию.
    /// </summary>
    public sealed partial class App : Application
    {
        private TransitionCollection transitions;

        public App()
        {
      
            this.Suspending += this.OnSuspending;
            this.Resuming += this.OnResuming;
            HardwareButtons.BackPressed += HardwareButtons_BackPressed;
            BackgroundMediaPlayer.MessageReceivedFromBackground += BackgroundMediaPlayer_MessageReceivedFromBackground;
            BackgroundMediaPlayer.Current.CurrentStateChanged += Current_CurrentStateChanged;
        }

        private void Current_CurrentStateChanged(MediaPlayer sender, object args)
        {
            switch (sender.CurrentState)
            {
                case MediaPlayerState.Opening:
                case MediaPlayerState.Playing: MainPage.ChangeEnableButtonPlayer(); break;
                case MediaPlayerState.Stopped:
                case MediaPlayerState.Paused: MainPage.ChangeEnableButtonPlayer(); break;
            }
        }

        private void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            Frame rootFrame = Window.Current.Content as Frame;
            if (rootFrame != null && rootFrame.CanGoBack)
            {
                e.Handled = true; rootFrame.GoBack();
            }
        }
        private void BackgroundMediaPlayer_MessageReceivedFromBackground(object sender, MediaPlayerDataReceivedEventArgs e)
        {
            foreach (var obj in e.Data)
            {
                var value = obj.Value;
                ViewModel.BusinessLayer.PlayerCommands command;
                Enum.TryParse(obj.Key, out command);
                switch (command)
                {
                    case ViewModel.BusinessLayer.PlayerCommands.Start:
                        AppCore.ViewModel_PropertiesActiveSong.SetActiveFileWithoutPlay(AppCore.ViewModel_LocalData.ListFiles.FirstOrDefault(x => x.Path.Equals(value.ToString())));
                        MainPage.ChangeEnableButtonPlayer();
                        AppCore.ViewModel_ListSongs.ChangeSong_IsActive();
                        break;
                }
            }
        }
        /// <summary>
        /// Вызывается при обычном запуске приложения пользователем.  Будут использоваться другие точки входа,
        /// если приложение запускается для открытия конкретного файла, отображения
        /// результатов поиска и т. д.
        /// </summary>
        /// <param name="e">Сведения о запросе и обработке запуска.</param>
        protected async override void OnLaunched(LaunchActivatedEventArgs e)
        {
#if DEBUG
            if (System.Diagnostics.Debugger.IsAttached)
            {
                this.DebugSettings.EnableFrameRateCounter = true;
            }
#endif

            Frame rootFrame = Window.Current.Content as Frame;

            // Не повторяйте инициализацию приложения, если в окне уже имеется содержимое,
            // только обеспечьте активность окна
            if (rootFrame == null)
            {
                // Создание фрейма, который станет контекстом навигации, и переход к первой странице
                rootFrame = new Frame();

                // TODO: Измените это значение на размер кэша, подходящий для вашего приложения
                rootFrame.CacheSize = 1;

                // Задайте язык по умолчанию
                rootFrame.Language = Windows.Globalization.ApplicationLanguages.Languages[0];

                if (e.PreviousExecutionState == ApplicationExecutionState.Terminated)
                {
                    // TODO: Загрузить состояние из ранее приостановленного приложения
                }

                // Размещение фрейма в текущем окне
                Window.Current.Content = rootFrame;
            }

            if (rootFrame.Content == null)
            {
                // Удаляет турникетную навигацию для запуска.
                if (rootFrame.ContentTransitions != null)
                {
                    this.transitions = new TransitionCollection();
                    foreach (var c in rootFrame.ContentTransitions)
                    {
                        this.transitions.Add(c);
                    }
                }

                rootFrame.ContentTransitions = null;
                rootFrame.Navigated += this.RootFrame_FirstNavigated;

                // Если стек навигации не восстанавливается для перехода к первой странице,
                // настройка новой страницы путем передачи необходимой информации в качестве параметра
                // навигации
                if (!rootFrame.Navigate(typeof(WindowsPhone.MainPage), e.Arguments))
                {
                    throw new Exception("Failed to create initial page");
                }
            }
            ApplicationSettingsHelper.SaveSettingsValue(ApplicationStates.AppState, ApplicationStates.ForegroundAppActive);
            // Обеспечение активности текущего окна
            Window.Current.Activate();
        
            await AppCore.InitializationData();
        }

        /// <summary>
        /// Восстанавливает переходы содержимого после запуска приложения.
        /// </summary>
        /// <param name="sender">Объект, где присоединен обработчик.</param>
        /// <param name="e">Сведения о событии перехода.</param>
        private void RootFrame_FirstNavigated(object sender, NavigationEventArgs e)
        {
            var rootFrame = sender as Frame;
            rootFrame.ContentTransitions = this.transitions ?? new TransitionCollection() { new NavigationThemeTransition() };
            rootFrame.Navigated -= this.RootFrame_FirstNavigated;
        }

        /// <summary>
        /// Вызывается при приостановке выполнения приложения.  Состояние приложения сохраняется
        /// без учета информации о том, будет ли оно завершено или возобновлено с неизменным
        /// содержимым памяти.
        /// </summary>
        /// <param name="sender">Источник запроса приостановки.</param>
        /// <param name="e">Сведения о запросе приостановки.</param>
        private void OnSuspending(object sender, SuspendingEventArgs e)
        {
            var deferral = e.SuspendingOperation.GetDeferral();
            ApplicationSettingsHelper.SaveSettingsValue(ApplicationStates.AppState, ApplicationStates.ForegroundAppSuspended);
            var message = new ValueSet();
            message.Add(ViewModel.BusinessLayer.PlayerCommands.Start.ToString(), AppCore.ViewModel_PropertiesActiveSong.ActiveFile.Path);
            BackgroundMediaPlayer.SendMessageToBackground(message);
            // TODO: Сохранить состояние приложения и остановить все фоновые операции
            deferral.Complete();
        }
        private void OnResuming(object sender, object e)
        {
            ApplicationSettingsHelper.SaveSettingsValue(ApplicationStates.AppState, ApplicationStates.ForegroundAppActive);
        }
    }
}